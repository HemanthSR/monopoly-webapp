/*
 * GET home page.
 */

exports.index = function (req, res) {
    res.render('index.jade');
};

exports.client = function (req, res) {
    var name = req.params.name;
    res.render('client/' + name);
};

exports.server = function (req, res) {
    var name = req.params.name;
    res.render('server/' + name);
};