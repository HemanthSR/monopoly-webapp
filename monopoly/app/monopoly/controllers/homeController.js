"use strict";

angular.module('monopoly.game')
    .controller('HomeController', ['$scope', '$location', '$rootScope',
        function ($scope, $location, $rootScope) {
            $scope.noOfPlayers = 0;


            $scope.createPlayers = function () {
                $scope.players = [];
                for (var i = 0; i < $scope.noOfPlayers; i++) {
                    $scope.players.push(new Monopoly.Game.Player("Player " + (i + 1)));
                }
            };

            $scope.startGame = function () {
                $rootScope.players = $scope.players;
                $location.url("/start");
            };
        }
    ]);
