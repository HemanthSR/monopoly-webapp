angular.module('monopoly.game')
    .controller('GameController', ['$scope', '$location', '$rootScope',
        function ($scope, $location, $rootScope) {
            var currentPlayerIndex = 0;
            $scope.utility = new Monopoly.Game.Utility();
            $scope.showOption = {"Buy": false, "Rent": false, "Tax": false, "CommunityChest": false};

            var addMessage = function (message) {
                    $scope.messages.push(message)
                },

                decideTurn = function () {
                    $scope.currentPlayer = $scope.players[currentPlayerIndex];
                    $scope.currentPlayer.setHisTurn(true);
                    currentPlayerIndex++;
                    if (currentPlayerIndex == $scope.players.length) {
                        currentPlayerIndex = 0;
                    }
                    addMessage("It's \"" + $scope.currentPlayer.name + "'s\" turn.");
                },

                initialization = function () {
                    $scope.players = $rootScope.players;
                    $scope.properties = Monopoly.Game.Property.createProperties();
                    $scope.communityChestCards = Monopoly.Game.Card.createCommunityChestCards();
                    $scope.messages = [];
                    groupProperties();
                    addMessage("Game Started.");
                    decideTurn();
                },

                checkPlayerHasToDoSomething = function () {
                    if ($scope.properties[$scope.currentPlayer.getPosition()].isBuyable()) {
                        $scope.showOption["Buy"] = true;
                    } else if ($scope.properties[$scope.currentPlayer.getPosition()].isRentable() &&
                        $scope.currentPlayer != $scope.properties[$scope.currentPlayer.getPosition()].getOwner()) {
                        $scope.showOption["Rent"] = true;
                    } else if ($scope.properties[$scope.currentPlayer.getPosition()].isTaxField()) {
                        $scope.showOption["Tax"] = true;
                    } else if ($scope.properties[$scope.currentPlayer.getPosition()].isCommunityChest()) {
                        $scope.showOption["CommunityChest"] = true;
                    }
                },

                resetOptions = function () {
                    $scope.showOption["Buy"] = false;
                    $scope.showOption["Rent"] = false;
                    $scope.showOption["Tax"] = false;
                    $scope.showOption["CommunityChest"] = false;
                },

                groupProperties = function () {
                    var groupPropertyArray = [];
                    var groupNumber;
                    for (var property in $scope.properties) {
                        groupNumber = $scope.properties[property].getGroupNumber();

                        if (groupNumber > 0) {
                            if (!groupPropertyArray[groupNumber]) {
                                groupPropertyArray[groupNumber] = [];
                            }
                            groupPropertyArray[groupNumber].push($scope.properties[property]);
                            $scope.properties[property].setGroup(groupPropertyArray[groupNumber]);
                        }
                    }
                };

            initialization();

            $scope.rollDice = function () {
                $scope.dice1 = $scope.utility.generateRandomNumberBW1and6();
                $scope.dice2 = $scope.utility.generateRandomNumberBW1and6();
                $scope.currentPlayer.rolledDice($scope.dice1, $scope.dice2);
                addMessage($scope.currentPlayer.name + " rolled " + ($scope.dice1 + $scope.dice2) + ".");
                if ($scope.dice1 == $scope.dice2) {
                    addMessage($scope.currentPlayer.name + " rolled doubles.")
                }
                addMessage($scope.currentPlayer.name + " landed on " + $scope.properties[$scope.currentPlayer.getPosition()].getName() + ".");
                checkPlayerHasToDoSomething();
            };

            $scope.endTurn = function () {
                $scope.currentPlayer.endTurn();
                decideTurn();
            };

            $scope.buy = function () {
                addMessage($scope.currentPlayer.name + " bought " + $scope.properties[$scope.currentPlayer.getPosition()].getName() + ".");
                $scope.currentPlayer.buy($scope.properties[$scope.currentPlayer.getPosition()]);
                $scope.properties[$scope.currentPlayer.getPosition()].setOwner($scope.currentPlayer);
                resetOptions();
            };

            $scope.payRent = function () {
                $scope.currentPlayer.payRent($scope.properties[$scope.currentPlayer.getPosition()]);
                addMessage($scope.currentPlayer.name + " payed £" + $scope.properties[$scope.currentPlayer.getPosition()].getRent() + " to " + $scope.properties[$scope.currentPlayer.getPosition()].getRent().name);
                resetOptions();
            };

            $scope.payTax = function () {
                $scope.currentPlayer.payTax($scope.properties[$scope.currentPlayer.getPosition()].getPrice());
                addMessage($scope.currentPlayer.name + " payed £" + $scope.properties[$scope.currentPlayer.getPosition()].getPrice() + " to Bank");
                resetOptions();
            };

            $scope.drawCommunityChestCard = function () {
                var communityChestCard = $scope.communityChestCards[$scope.utility.generateRandomNumberBW1and15()];
                addMessage($scope.currentPlayer.name + " drew " + "\"" + communityChestCard.name + "\"");
                communityChestCard.action($scope.communityChestActions);
                resetOptions();
            };

            $scope.communityChestActions = {
                incrementMoney: function (_money) {
                    $scope.currentPlayer.incrementMoney(_money);
                },

                decrementMoney: function (_money) {
                    $scope.currentPlayer.decrementMoney(_money);
                },

                addCommunityChestJailCard: function (_card) {
                    $scope.currentPlayer.addCommunityChestJailCard(_card);
                },

                setPosition: function (_position) {
                    $scope.currentPlayer.setPosition(_position);
                },

                collectFromEachPlayer: function (_money) {
                    $scope.players.forEach(function (player) {
                        if (player != $scope.currentPlayer) {
                            player.decrementMoney(10);
                            $scope.currentPlayer.incrementMoney(10);
                        }
                    });
                },

                goToJail: function () {
                    //TODO: jail part is to be done.
                },

                streetRepair: function (perHouse, perHotel) {
                    $scope.currentPlayer.decrementMoney($scope.currentPlayer.getStreetRepairsMoney(perHouse, perHotel));
                }
            };
        }
    ]);
