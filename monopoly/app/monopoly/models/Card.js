Monopoly.Game.Card = function (name, action) {
    this.name = name;
    this.action = action;
};

Monopoly.Game.Card.createCommunityChestCards = function () {
    var communityChestCards = [];
    communityChestCards.push(new Monopoly.Game.Card("Get out of Jail, Free. This card may be kept until needed or sold.", function (actions) {
        actions.addCommunityChestJailCard(this);
    }));
    communityChestCards.push(new Monopoly.Game.Card("You have won second prize in a beauty contest. Collect £10.", function (actions) {
        actions.incrementMoney(10);
    }));
    communityChestCards.push(new Monopoly.Game.Card("From sale of stock, you get £50.", function (actions) {
        actions.incrementMoney(50);
    }));
    communityChestCards.push(new Monopoly.Game.Card("Life insurance matures. Collect £100.", function (actions) {
        actions.incrementMoney(100);
    }));
    communityChestCards.push(new Monopoly.Game.Card("Income tax refund. Collect £20.", function (actions) {
        actions.incrementMoney(20);
    }));
    communityChestCards.push(new Monopoly.Game.Card("Holiday fund matures. Receive £100.", function (actions) {
        actions.incrementMoney(100);
    }));
    communityChestCards.push(new Monopoly.Game.Card("You inherit £100.", function (actions) {
        actions.incrementMoney(100);
    }));
    communityChestCards.push(new Monopoly.Game.Card("Receive £25 consultancy fee.", function (actions) {
        actions.incrementMoney(25);
    }));
    communityChestCards.push(new Monopoly.Game.Card("Pay hospital fees of £100.", function (actions) {
        actions.decrementMoney(100);
    }));
    communityChestCards.push(new Monopoly.Game.Card("Bank error in your favor. Collect £200.", function (actions) {
        actions.incrementMoney(200);
    }));
    communityChestCards.push(new Monopoly.Game.Card("Pay school fees of £50.", function (actions) {
        actions.decrementMoney(50);
    }));
    communityChestCards.push(new Monopoly.Game.Card("Doctor's fee. Pay £50.", function (actions) {
        actions.decrementMoney(50);
    }));
    communityChestCards.push(new Monopoly.Game.Card("It is your birthday. Collect £10 from every actions.", function (actions) {
        actions.collectFromEachPlayer(10);
    }));
    communityChestCards.push(new Monopoly.Game.Card("Advance to \"GO\" (Collect £200).", function (actions) {
        actions.incrementMoney(200);
        actions.setPosition(0);
    }));
    communityChestCards.push(new Monopoly.Game.Card("You are assessed for street repairs. £40 per house. £115 per hotel.", function (actions) {
        actions.streetRepair(40, 115);
    }));
    communityChestCards.push(new Monopoly.Game.Card("Go to Jail. Go directly to Jail. Do not pass \"GO\". Do not collect £200.", function (actions) {
        actions.goToJail();
    }));

    return communityChestCards;
};

Monopoly.Game.Card.createChanceCards = function () {
    var chanceCards = [];
    chanceCards.push(new Monopoly.Game.Card("GET OUT OF JAIL FREE. This card may be kept until needed or traded.", function (actions) {
        actions.addChanceJailCard(this);
    }));
    chanceCards.push(new Monopoly.Game.Card("Make General Repairs on All Your Property. For each house pay £25. For each hotel £100.", function (actions) {
        actions.decrementMoney(actions.getStreetRepairsMoney(25, 100));
    }));
    chanceCards.push(new Monopoly.Game.Card("Speeding fine £15.", function (actions) {
        actions.decrementMoney(15);
    }));
    chanceCards.push(new Monopoly.Game.Card("You have been elected chairman of the board. Pay each actions £50.", function (actions) {
    }));
    chanceCards.push(new Monopoly.Game.Card("Go back three spaces.", function (actions) {
        actions.decrementPosition(3);
    }));
    chanceCards.push(new Monopoly.Game.Card("ADVANCE TO THE NEAREST UTILITY. IF UNOWNED, you may buy it from the Bank. IF OWNED, throw dice and pay owner a total ten times the amount thrown.", function (actions) {
    }));
    chanceCards.push(new Monopoly.Game.Card("Bank pays you dividend of £50.", function (actions) {
        actions.incrementMoney(50);
    }));
    chanceCards.push(new Monopoly.Game.Card("ADVANCE TO THE NEAREST RAILROAD. If UNOWNED, you may buy it from the Bank. If OWNED, pay owner twice the rental to which they are otherwise entitled.", function (actions) {

    }));
    chanceCards.push(new Monopoly.Game.Card("Pay poor tax of £15.", function (actions) {
        actions.decrementMoney(15);
    }));
    chanceCards.push(new Monopoly.Game.Card("Take a trip to Reading Rail Road. If you pass \"GO\" collect £200.", function (actions) {

    }));
    chanceCards.push(new Monopoly.Game.Card("ADVANCE to Boardwalk.", function (actions) {
    }));
    chanceCards.push(new Monopoly.Game.Card("ADVANCE to Illinois Avenue. If you pass \"GO\" collect £200.", function (actions) {
    }));
    chanceCards.push(new Monopoly.Game.Card("Your building loan matures. Collect £150.", function (actions) {
        actions.incrementMoney(150);
    }));
    chanceCards.push(new Monopoly.Game.Card("ADVANCE TO THE NEAREST RAILROAD. If UNOWNED, you may buy it from the Bank. If OWNED, pay owner twice the rental to which they are otherwise entitled.", function (actions) {
    }));
    chanceCards.push(new Monopoly.Game.Card("ADVANCE to St. Charles Place. If you pass \"GO\" collect £200.", function (actions) {
    }));
    chanceCards.push(new Monopoly.Game.Card("Go to Jail. Go Directly to Jail. Do not pass \"GO\". Do not collect £200.", function (actions) {
    }));
    return chanceCards;
};