Monopoly.Game.Player = function (_name) {
    this.name = _name;
    this.color = null;
    this.money = 1500;
    var position = 0;
    var inJail = false;
    var jailRoll = 0;
    var communityChestJailCard = [];
    var chanceJailCard = [];
    var hisTurn = false;
    var isHeRolled = false;
    var doubleRoll = 0;
    var ownedProperties = [];

    this.setHisTurn = function (value) {
        hisTurn = value;
    };

    this.isItHisTurn = function () {
        return hisTurn;
    };

    this.isHeRolled = function () {
        return isHeRolled;
    };

    this.getName = function () {
        return this.name;
    };

    this.getMoney = function () {
        return this.money;
    };

    this.isHeInJail = function () {
        return inJail;
    };

    this.getJailRoll = function () {
        return jailRoll;
    };

    this.getCommunityChestJailCard = function () {
        return communityChestJailCard;
    };

    this.getChanceJailCard = function () {
        return chanceJailCard;
    };

    this.getDoubleRolls = function () {
        return doubleRoll;
    };

    this.getOwnedProperties = function () {
        return ownedProperties;
    };

    this.getPosition = function () {
        return position;
    };

    this.setPosition = function (_position) {
        position = _position;
    };

    var incrementPosition = function (pos) {
        position = (position + pos) % 40;
    };

    var checkThePlayerRolledDoubles = function (dice1, dice2) {
        if (dice1 == dice2) {
            doubleRoll = doubleRoll + 1;
        } else {
            doubleRoll = 0;
            isHeRolled = true;
        }
    };

    this.checkThePlayerCrossedGo = function (previousPosition) {
        if (previousPosition > position) {
            if (position == 0) {
                this.incrementMoney(400);
            } else {
                this.incrementMoney(200);
            }
        }
    };

    this.rolledDice = function (dice1, dice2) {
        var previousPosition = position;

        incrementPosition(dice1 + dice2);

        this.checkThePlayerCrossedGo(previousPosition);

        checkThePlayerRolledDoubles(dice1, dice2);
    };

    this.endTurn = function () {
        isHeRolled = false;
        hisTurn = false;
    };

    this.incrementMoney = function (money) {
        this.money = this.money + money;
    };

    this.decrementMoney = function (money) {
        this.money = this.money - money;
    };

    this.buy = function (property) {
        ownedProperties.push(property);
        this.decrementMoney(property.getPrice());
    };

    this.payRent = function (property) {
        var rentOfTheProperty = property.getRent();
        this.decrementMoney(rentOfTheProperty);
        property.getOwner().incrementMoney(rentOfTheProperty);
    };

    this.payTax = function (taxPrice) {
        this.decrementMoney(taxPrice);
    };

    this.addCommunityChestJailCard = function (card) {
        communityChestJailCard.push(card);
    };

    this.getStreetRepairsMoney = function (perHouse, perHotel) {
        var noOfHotels = 0;
        var noOfHouses = 0;
        ownedProperties.forEach(function (property) {
            noOfHotels += property.getNoOfHotels();
            noOfHouses += property.getNoOfHouses();
        });

        return ((noOfHouses * perHouse) + (noOfHotels * perHotel));
    };

    this.decrementPosition = function (_position) {
        position -= _position;
    };
};