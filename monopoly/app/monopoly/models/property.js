Monopoly.Game.Property = function (_propertyNumber, _name, _pricetext, _price, _groupNumber, baserent, rentWith1House, rentWith2Houses, rentWith3Houses, rentWith4Houses, _rentWithHotel) {
    var propertyNumber = _propertyNumber;
    var name = _name;
    var pricetext = _pricetext || '';
    var owner = null;
    var mortgage = false;
    var noOfHouses = 0;
    var noOfHotels = 0;
    var groupNumber = _groupNumber || 0;
    var price = (_price || 0);
    var rent = {
        0: baserent || 0,
        1: rentWith1House || 0,
        2: rentWith2Houses || 0,
        3: rentWith3Houses || 0,
        4: rentWith4Houses || 0
    };

    var rentOfRailwayStation = {
        1: 50,
        2: 100,
        3: 150,
        4: 200
    };

    var rentOfGovernmentProperty = {
        1: 75,
        2: 150
    };

    var rentWithHotel = (_rentWithHotel || 0);
    var group;
    var housePrice = {
        3: 50,
        4: 50,
        5: 100,
        6: 100,
        7: 150,
        8: 150,
        9: 200,
        10: 200
    };

    this.buildBuilding = function () {
        if (noOfHouses < 4)
            noOfHouses++;
        else {
            noOfHotels++;
            noOfHouses = 0;
        }
    };

    this.getName = function () {
        return name;
    };

    this.getPrice = function () {
        return price;
    };

    this.isBuyable = function () {
        return price != 0 && owner == null && groupNumber > 0;
    };

    this.isRentable = function () {
        return baserent != 0 && owner != null
    };

    this.getGroupNumber = function () {
        return groupNumber;
    };

    this.setGroup = function (propertiesBelongsToThisGroup) {
        group = propertiesBelongsToThisGroup;
    };

    this.getGroup = function () {
        return group;
    };

    this.getPropertyNumber = function () {
        return propertyNumber;
    };

    this.getClass = function () {
        return "property" + propertyNumber;
    };

    this.setOwner = function (_owner) {
        owner = _owner
    };

    this.getOwner = function () {
        return owner;
    };

    this.getRent = function () {
        if (groupNumber == 1 || groupNumber == 2) {
            var noOfProperty = 0;
            for (var groupMember in group) {
                if (group[groupMember].getOwner() == owner) {
                    noOfProperty++;
                }
            }
            return groupNumber == 1 ? rentOfRailwayStation[noOfProperty] : rentOfGovernmentProperty[noOfProperty];
        }
        return noOfHotels == 1 ? rentWithHotel : rent[noOfHouses];
    };

    this.isTaxField = function () {
        return groupNumber == -1;
    };

    this.getNoOfHouses = function () {
        return noOfHouses;
    };

    this.getNoOfHotels = function () {
        return noOfHotels;
    };

    this.isCommunityChest = function () {
        return groupNumber == -2;
    };
};


Monopoly.Game.Property.createProperties = function () {
    var properties = [];
    properties.push(new Monopoly.Game.Property(0, "GO", "COLLECT £200 SALARY AS YOU PASS."));
    properties.push(new Monopoly.Game.Property(1, "Old Kent Road", "£60", 60, 3, 2, 10, 30, 90, 160, 250));
    properties.push(new Monopoly.Game.Property(2, "Community Chest", "FOLLOW INSTRUCTIONS ON TOP CARD", 0, -2));
    properties.push(new Monopoly.Game.Property(3, "Whitechapel Road", "£60", 60, 3, 4, 20, 60, 180, 320, 450));
    properties.push(new Monopoly.Game.Property(4, "City Tax", "Pay £200", 200, -1));
    properties.push(new Monopoly.Game.Property(5, "King Cross Station", "£200", 200, 1));
    properties.push(new Monopoly.Game.Property(6, "The Angle Islington", "£100", 100, 4, 6, 30, 90, 270, 400, 550));
    properties.push(new Monopoly.Game.Property(7, "Chance", "FOLLOW INSTRUCTIONS ON TOP CARD"));
    properties.push(new Monopoly.Game.Property(8, "Euston Road", "£100", 100, 4, 6, 30, 90, 270, 400, 550));
    properties.push(new Monopoly.Game.Property(9, "Pentonville Road", "£120", 120, 4, 8, 40, 100, 300, 450, 600));
    properties.push(new Monopoly.Game.Property(10, "Just Visiting"));
    properties.push(new Monopoly.Game.Property(11, "Pall Mall", "£140", 140, 5, 10, 50, 150, 450, 625, 750));
    properties.push(new Monopoly.Game.Property(12, "Electric Company", "£150", 150, 2));
    properties.push(new Monopoly.Game.Property(13, "White Hall", "£140", 140, 5, 10, 50, 150, 450, 625, 750));
    properties.push(new Monopoly.Game.Property(14, "Northumberl'D Avenue", "£160", 160, 5, 12, 60, 180, 500, 700, 900));
    properties.push(new Monopoly.Game.Property(15, "Marylebone Station", "£200", 200, 1));
    properties.push(new Monopoly.Game.Property(16, "Bow Street", "£180", 180, 6, 14, 70, 200, 550, 750, 950));
    properties.push(new Monopoly.Game.Property(17, "Community Chest", "FOLLOW INSTRUCTIONS ON TOP CARD", 0, -2));
    properties.push(new Monopoly.Game.Property(18, "Marlborough Street", "£180", 180, 6, 14, 70, 200, 550, 750, 950));
    properties.push(new Monopoly.Game.Property(19, "Vine Street", "£200", 200, 6, 16, 80, 220, 600, 800, 1000));
    properties.push(new Monopoly.Game.Property(20, "Free Parking"));
    properties.push(new Monopoly.Game.Property(21, "The Strand", "£220", 220, 7, 18, 90, 250, 700, 875, 1050));
    properties.push(new Monopoly.Game.Property(22, "Chance", "FOLLOW INSTRUCTIONS ON TOP CARD"));
    properties.push(new Monopoly.Game.Property(23, "Fleet Street", "£220", 220, 7, 18, 90, 250, 700, 875, 1050));
    properties.push(new Monopoly.Game.Property(24, "Trafalgar Square", "£240", 240, 7, 20, 100, 300, 750, 925, 1100));
    properties.push(new Monopoly.Game.Property(25, "Fenchurch Street Station", "£200", 200, 1));
    properties.push(new Monopoly.Game.Property(26, "Leicester Square", "£260", 260, 8, 22, 110, 330, 800, 975, 1150));
    properties.push(new Monopoly.Game.Property(27, "Coventry Street", "£260", 260, 8, 22, 110, 330, 800, 975, 1150));
    properties.push(new Monopoly.Game.Property(28, "Water Works", "£150", 150, 2));
    properties.push(new Monopoly.Game.Property(29, "Piccadilly", "£280", 280, 8, 24, 120, 360, 850, 1025, 1200));
    properties.push(new Monopoly.Game.Property(30, "Go to Jail", "Go directly to Jail. Do not pass GO. Do not collect £200."));
    properties.push(new Monopoly.Game.Property(31, "Regent Street", "£300", 300, 9, 26, 130, 390, 900, 110, 1275));
    properties.push(new Monopoly.Game.Property(32, "Oxford Street", "£300", 300, 9, 26, 130, 390, 900, 110, 1275));
    properties.push(new Monopoly.Game.Property(33, "Community Chest", "FOLLOW INSTRUCTIONS ON TOP CARD", 0, -2));
    properties.push(new Monopoly.Game.Property(34, "Bond Street", "£320", 320, 9, 28, 150, 450, 1000, 1200, 1400));
    properties.push(new Monopoly.Game.Property(35, "Liverpool Street Station", "£200", 200, 1));
    properties.push(new Monopoly.Game.Property(36, "Chance", "FOLLOW INSTRUCTIONS ON TOP CARD"));
    properties.push(new Monopoly.Game.Property(37, "Park Lane", "£350", 350, 10, 35, 175, 500, 1100, 1300, 1500));
    properties.push(new Monopoly.Game.Property(38, "LUXURY TAX", "Pay £100", 100, -1));
    properties.push(new Monopoly.Game.Property(39, "Mayfair", "£400", 400, 10, 50, 200, 600, 1400, 1700, 2000));
    return properties;
};
