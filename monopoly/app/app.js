'use strict';

angular.module('monopoly', ['ngRoute', 'monopoly.game'])
    .config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
    $routeProvider.when('/', {templateUrl: 'monopoly/views/home.html', controller: 'HomeController'});
    $routeProvider.when('/start', {templateUrl: 'monopoly/views/game.html', controller: 'GameController'});
    }]);