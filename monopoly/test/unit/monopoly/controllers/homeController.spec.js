'use strict';

describe("Monopoly : ", function () {
    var scope;
    var homeController;
    var _location;
    var rootScope;

    beforeEach(module('monopoly.game'));
    beforeEach(inject(['$rootScope', '$location', function ($rootScope, $location) {
        scope = $rootScope.$new();
        _location = $location;
        rootScope = $rootScope;
    }]));

    var setUpController = function () {
        inject(function ($controller) {
            homeController = $controller('HomeController', {
                $scope: scope,
                $location: _location,
                $rootScope: rootScope
            });
        });
    };

    describe("create player", function () {
        beforeEach(function () {
            setUpController();
        });

        it("should create 2 player if the noOfPlayers are 2", function () {
            scope.noOfPlayers = 2;
            scope.createPlayers();

            expect(scope.players.length).toBe(2);
        });

        it("should create player with name \'player 1\' and \'player 2\'", function () {
            scope.noOfPlayers = 2;
            scope.createPlayers();

            expect(scope.players.length).toBe(2);
            expect(scope.players[0].getName()).toBe("Player 1");
            expect(scope.players[1].getName()).toBe("Player 2");
            expect(scope.players[2]).toBe(undefined);
        });
    });

    describe("start game", function () {
        beforeEach(function () {
            setUpController();
        });

        it("should set players to the root scope", function () {
            scope.noOfPlayers = 2;
            scope.createPlayers();
            scope.startGame();

            expect(rootScope.players).toNotBe(undefined);
        });

        it("should forward the url", function () {
            spyOn(_location, 'url');
            scope.startGame();

            expect(_location.url).toHaveBeenCalledWith("/start");
        });
    })
});