'use strict';

describe("Monopoly : ", function () {
    var scope;
    var gameController;
    var _location;
    var rootScope;

    beforeEach(module('monopoly.game'));
    function getPlayers() {
        var players = [];
        for (var i = 0; i < 2; i++) {
            players.push(new Monopoly.Game.Player("Player " + (i + 1)));
        }
        return players;
    }

    beforeEach(inject(['$rootScope', '$location', function ($rootScope, $location) {
        scope = $rootScope.$new();
        _location = $location;
        rootScope = $rootScope;
        rootScope.players = getPlayers();
        scope.properties = Monopoly.Game.Property.createProperties();
        scope.communityChestCards = Monopoly.Game.Card.createCommunityChestCards();
    }]));

    var setUpController = function () {
        inject(function ($controller) {
            gameController = $controller('GameController', {
                $scope: scope,
                $location: _location,
                $rootScope: rootScope
            });
        });
    };

    describe("Game Controller", function () {
        describe("initialization", function () {
            beforeEach(function () {
                setUpController();
            });

            it("should set players and properties to scope from rootScope", function () {
                expect(scope.players).toBeDefined();
                expect(scope.players.length).toBe(2);
                expect(scope.properties).toBeDefined();
                expect(scope.properties.length).toBe(40);
            });

            it("should add a message called \'Game Started.\'", function () {
                expect(scope.messages).toBeDefined();
                expect(scope.messages.length).toBe(2);
                expect(scope.messages[0]).toBe("Game Started.");
            });

            it("should decide turn and set current player", function () {
                expect(scope.currentPlayer).toBeDefined();
                expect(scope.currentPlayer).toBe(scope.players[0]);
            });
        });

        describe("Roll Dice", function () {
            beforeEach(function () {
                setUpController();
            });

            it("should assign value to dice1 and dice2", function () {
                scope.rollDice();

                expect(scope.dice1).toBeDefined();
                expect(scope.dice2).toBeDefined();
            });

            it("should change the players position to the 4 if he rolled 4", function () {
                spyOn(scope.utility, 'generateRandomNumberBW1and6').andReturn(2);

                scope.rollDice();

                expect(scope.currentPlayer.getPosition()).toBe(4);
                expect(scope.currentPlayer.isItHisTurn()).toBe(true)
            });

            it("should not change the current player if he rolled doubles", function () {
                spyOn(scope.utility, 'generateRandomNumberBW1and6').andReturn(2);

                scope.rollDice();

                expect(scope.players[0]).toBe(scope.currentPlayer);
                expect(scope.players[0].getDoubleRolls()).toBe(1);
                expect(scope.players[0].isItHisTurn()).toBe(true);
            });

            it("should set show option to true for buy if the player landed on buyable property", function () {

                spyOn(scope.utility, 'generateRandomNumberBW1and6').andReturn(3);

                scope.rollDice();

                expect(scope.showOption.Buy).toBe(true);
            });

            it("should not set show option to true for buy if the player landed on tax property", function () {
                spyOn(scope.utility, 'generateRandomNumberBW1and6').andReturn(2);

                scope.rollDice();

                expect(scope.showOption.Buy).toBe(false);
            });

            it("should set show option to true for Rent if the player landed on property which is owned by other player", function () {
                spyOn(scope.utility, 'generateRandomNumberBW1and6').andReturn(3);
                scope.properties[6].setOwner(scope.players[1]);

                scope.rollDice();

                expect(scope.showOption.Rent).toBe(true);
            });

            it("should set show option to true for Community Chest if the player landed on Community Chest", function () {
                spyOn(scope.utility, 'generateRandomNumberBW1and6').andReturn(1);

                scope.rollDice();

                expect(scope.showOption.CommunityChest).toBe(true);
            });

            it("should not set show option to true for Community Chest if the player landed on other properties", function () {
                spyOn(scope.utility, 'generateRandomNumberBW1and6').andReturn(2);

                scope.rollDice();

                expect(scope.showOption.CommunityChest).toBe(false);
            });
        });

        describe("Buy", function () {
            beforeEach(function () {
                setUpController();
            });

            it("should add the properties to the player's owned properties and reduce players money", function () {
                spyOn(scope.utility, 'generateRandomNumberBW1and6').andReturn(3);
                scope.rollDice();
                expect(scope.showOption.Buy).toBe(true);

                scope.buy();

                expect(scope.showOption.Buy).toBe(false);
                expect(scope.currentPlayer.getOwnedProperties()[0]).toBe(scope.properties[6]);
                expect(scope.currentPlayer.getMoney()).toBe(1500 - scope.properties[6].getPrice());
            });
        });

        describe("Rent", function () {
            beforeEach(function () {
                setUpController();
            });

            it("should reduce player's money by the rent of the property", function () {
                spyOn(scope.utility, 'generateRandomNumberBW1and6').andReturn(3);
                scope.rollDice();
                scope.buy();
                scope.endTurn();
                scope.rollDice();
                scope.payRent();

                expect(scope.currentPlayer.getMoney()).toBe(1500 - scope.properties[6].getRent());
            });

            it("should increase the owner's money by the rent of the property", function () {
                spyOn(scope.utility, 'generateRandomNumberBW1and6').andReturn(3);
                scope.rollDice();
                scope.buy();
                scope.endTurn();
                scope.rollDice();
                scope.payRent();

                expect(scope.players[0].getMoney()).toBe(1500 - scope.properties[6].getPrice() + scope.properties[6].getRent())
            })
        });

        describe("Tax", function () {
            beforeEach(function () {
                setUpController();
            });

            it("should reduce player's money by the rent of the tax", function () {
                spyOn(scope.utility, 'generateRandomNumberBW1and6').andReturn(2);
                scope.rollDice();
                scope.payTax();

                expect(scope.players[0].getMoney()).toBe(1500 - scope.properties[4].getPrice());
            });
        });

        describe("EndTurn", function () {
            beforeEach(function () {
                setUpController();
            });

            it("should change the current player and end the previous players turn", function () {
                expect(scope.players[0].isItHisTurn()).toBe(true);
                expect(scope.currentPlayer).toBe(scope.players[0]);

                scope.endTurn();

                expect(scope.players[0].isItHisTurn()).toBe(false);
                expect(scope.players[1].isItHisTurn()).toBe(true);
                expect(scope.currentPlayer).toBe(scope.players[1]);
            })
        });

        describe("Community Chest", function () {
            beforeEach(function () {
                setUpController();
            });

            it("should call action of the card if he draw a card", function () {
                spyOn(scope.utility, 'generateRandomNumberBW1and15').andReturn(1);
                scope.rollDice();
                scope.drawCommunityChestCard();

                expect(scope.players[0].getMoney()).toBe(1510);
            })
        });
    })
});