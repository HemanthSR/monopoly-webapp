'use strict';

describe("Monoploy: ", function () {
    var Player = Monopoly.Game.Player;
    var Property = Monopoly.Game.Property;

    describe("Player", function () {
        var player = new Player("name");
        it("should create with name passed in constructor", function () {
            expect(player.getName()).toBe("name");
        });

        it("should create with money 1500", function () {
            expect(player.getMoney()).toBe(1500);
        });

        it("should create with initial position", function () {
            expect(player.getPosition()).toBe(0);
        });

        it("should create with out of jail ", function () {
            expect(player.isHeInJail()).toBe(false);
        });

        it("should create with jailRoll 0", function () {
            expect(player.getJailRoll()).toBe(0);
        });

        it("should not have any community chest jail card while creating.", function () {
            expect(player.getCommunityChestJailCard().length).toBe(0);
        });

        it("should not have any chance jail card while creating.", function () {
            expect(player.getChanceJailCard().length).toBe(0);
        });

        it("should not be rolled dice while creating", function () {
            expect(player.isHeRolled()).toBe(false);
        });

        it("doubleRoll should be 0 while creating", function () {
            expect(player.getDoubleRolls()).toBe(0);
        });

        it("should not own any property while creating", function () {
            expect(player.getOwnedProperties().length).toBe(0);
        });

        describe("rolled dice", function () {

            it("should end his turn", function () {
                var player = new Player("name");
                player.rolledDice(1, 2);
                expect(player.isHeRolled()).toBe(true);
            });

            it("should not set is he rolled to true if he rolled doubles", function () {
                var player = new Player("name");
                player.rolledDice(1, 1);
                expect(player.isHeRolled()).toBe(false);
            });

            it("should increment the position", function () {
                var player = new Player("name");
                player.rolledDice(1, 1);
                expect(player.getPosition()).toBe(2);
            });

            it("should limit the position to 39. if he rolled more than tat it should reset.", function () {
                var player = new Player("name");
                player.rolledDice(20, 20);
                expect(player.getPosition()).toBe(0);
            });

            it("should increment the money by 400 if he landed on go.", function () {
                var player = new Player("name");
                player.rolledDice(10, 10);
                player.rolledDice(10, 10);
                expect(player.getMoney()).toBe(1900);
            });

            it("should increment the money by 200 if he crossed go.", function () {
                var player = new Player("name");
                player.rolledDice(10, 10);
                player.rolledDice(10, 11);
                expect(player.getMoney()).toBe(1700);
            });

            it("should increment the double roll if he rolled doubles.", function () {
                var player = new Player("name");
                player.rolledDice(10, 10);
                expect(player.getDoubleRolls()).toBe(1);
            });

            it("should reset the double roll to 0 if he don't roll doubles.", function () {
                var player = new Player("name");
                player.rolledDice(10, 10);
                expect(player.getDoubleRolls()).toBe(1);
                player.rolledDice(10, 11);
                expect(player.getDoubleRolls()).toBe(0);
            });
        });

        describe("end turn", function () {
            var player = new Player("name");
            it("should set is he rolled to false", function () {
                player.rolledDice(1, 2);
                expect(player.isHeRolled()).toBe(true);
                player.endTurn();
                expect(player.isHeRolled()).toBe(false);
            });
        });

        describe("buy", function () {
            var property = new Property(1, "Old Kent Road", "$60", 60, 3, 2, 10, 30, 90, 160, 250);
            it("should add the property to the owned properties", function () {
                var player = new Player("name");
                player.buy(property);
                expect(player.getOwnedProperties().length).toBe(1);
            });

            it("should reduce the money by the price of the property", function () {
                var player = new Player("name");
                player.buy(property);
                expect(player.getMoney()).toBe(1440);
            });
        });

        describe("pay rent", function () {
            var property = new Property(1, "Old Kent Road", "$60", 60, 3, 2, 10, 30, 90, 160, 250);
            var owner = new Player("owner");
            property.setOwner(owner);

            it("should reduce the money of the player by base rent if the property has no houses and hotel.", function () {
                var player = new Player("name");
                player.payRent(property);

                expect(player.getMoney()).toBe(1500 - 2);
            });

            it("should reduce the money of the player by rentwith1house if the property has one house and no hotel.", function () {
                property.buildBuilding();
                var player = new Player("name");
                player.payRent(property);

                expect(player.getMoney()).toBe(1500 - 10);
            });

            it("should reduce the money of the player by rentwith2house if the property has two houses and no hotel.", function () {
                property.buildBuilding();
                var player = new Player("name");
                player.payRent(property);

                expect(player.getMoney()).toBe(1500 - 30);
            });

            it("should reduce the money of the player by rentwith3house if the property has three houses and no hotel.", function () {
                property.buildBuilding();
                var player = new Player("name");
                player.payRent(property);

                expect(player.getMoney()).toBe(1500 - 90);
            });

            it("should reduce the money of the player by rentwith4house if the property has four houses and no hotel.", function () {
                property.buildBuilding();
                var player = new Player("name");
                player.payRent(property);

                expect(player.getMoney()).toBe(1500 - 160);
            });

            it("should reduce the money of the player by rentwithhotel if the property has a hotel.", function () {
                property.buildBuilding();
                var player = new Player("name");
                player.payRent(property);

                expect(player.getMoney()).toBe(1500 - 250);
            });
        });

        describe("pay rent", function () {
            var kingCross = new Monopoly.Game.Property(5, "King Cross Station", "$200", 200, 1);
            var marylebone = new Monopoly.Game.Property(15, "Marylebone Station", "$200", 200, 1);
            var fenchurchStreet = new Monopoly.Game.Property(25, "Fenchurch Street Station", "$200", 200, 1);
            var liverpool = new Monopoly.Game.Property(35, "Liverpool Street Station", "$200", 200, 1);

            var group = [kingCross, marylebone, fenchurchStreet, liverpool];
            kingCross.setGroup(group);
            marylebone.setGroup(group);
            fenchurchStreet.setGroup(group);
            liverpool.setGroup(group);

            var owner = new Player("owner");
            it("should reduce the money of the player by rentwith one railwaystation if the owner has no other railwaystation", function () {
                kingCross.setOwner(owner);
                var player = new Player("name");
                player.payRent(kingCross);

                expect(player.getMoney()).toBe(1500 - 50);
            });

            it("should reduce the money of the player by rentwith two railwaystation if the owner has two railwaystation", function () {
                kingCross.setOwner(owner);
                marylebone.setOwner(owner);
                var player = new Player("name");
                player.payRent(kingCross);

                expect(player.getMoney()).toBe(1500 - 100);
            });

            it("should reduce the money of the player by rentwith three railwaystation if the owner has three railwaystation", function () {
                kingCross.setOwner(owner);
                marylebone.setOwner(owner);
                fenchurchStreet.setOwner(owner);
                var player = new Player("name");
                player.payRent(kingCross);

                expect(player.getMoney()).toBe(1500 - 150);
            });

            it("should reduce the money of the player by rentwith four railwaystation if the owner has four railwaystation", function () {
                kingCross.setOwner(owner);
                marylebone.setOwner(owner);
                fenchurchStreet.setOwner(owner);
                liverpool.setOwner(owner);
                var player = new Player("name");
                player.payRent(kingCross);

                expect(player.getMoney()).toBe(1500 - 200);
            });
        });

        describe("pay rent", function () {
            var electricCompany = new Monopoly.Game.Property(12, "Electric Company", "$150", 150, 2);
            var waterworks = new Monopoly.Game.Property(28, "Water Works", "$150", 150, 2);

            var group = [electricCompany, waterworks];
            electricCompany.setGroup(group);
            waterworks.setGroup(group);

            var owner = new Player("owner");
            it("should reduce the money of the player by rentwith one government property if the owner has no other governament property", function () {
                electricCompany.setOwner(owner);
                var player = new Player("name");
                player.payRent(electricCompany);

                expect(player.getMoney()).toBe(1500 - 75);
            });

            it("should reduce the money of the player by rentwith two government property if the owner has two government property", function () {
                electricCompany.setOwner(owner);
                waterworks.setOwner(owner);
                var player = new Player("name");
                player.payRent(electricCompany);

                expect(player.getMoney()).toBe(1500 - 150);
            });
        });

        describe("pay Tax", function () {
            it("should reduce the money 200", function () {
                var player = new Player("name");
                player.payTax(200);

                expect(player.getMoney()).toBe(1500 - 200);
            });

            it("should reduce the money 100", function () {
                var player = new Player("name");
                player.payTax(100);

                expect(player.getMoney()).toBe(1500 - 100);
            });
        });
    });
});