'use strict';

describe("Monoploy: ", function () {
    var Player = Monopoly.Game.Player;
    var cards = Monopoly.Game.Card.createCommunityChestCards();
    var player;
    var players = [];
    players.push(new Player("new"));
    var communityChestActions = {
        incrementMoney: function (_money) {
            player.incrementMoney(_money);
        },

        decrementMoney: function (_money) {
            player.decrementMoney(_money);
        },

        addCommunityChestJailCard: function (_card) {
            player.addCommunityChestJailCard(_card);
        },

        setPosition: function (_position) {
            player.setPosition(_position);
        },

        collectFromEachPlayer: function (_money) {
            players.forEach(function (_player) {
                if (_player != player) {
                    player.incrementMoney(_money);
                    _player.decrementMoney(_money);
                }
            })
        },

        goToJail: function () {
            //TODO: jail part is to be done.
        },

        streetRepair: function (perHouse, perHotel) {
            player.decrementMoney(player.getStreetRepairsMoney(perHouse, perHotel));
        }

    };

    beforeEach(function () {
        player = new Player("name");
        players.push(player);
    });

    afterEach(function () {
        players.pop();
    });

    describe("Community Chest", function () {
        it("get out of jail card should add to the players community chest jail cards", function () {
            cards[0].action(communityChestActions);
            expect(player.getCommunityChestJailCard().length).toBe(1);
            expect(player.getCommunityChestJailCard()[0]).toBe(cards[0]);
        });

        it("won in beauty context should increase players money by 10", function () {
            cards[1].action(communityChestActions);
            expect(player.getMoney()).toBe(1510);
        });

        it("should get 50 from sale of stock", function () {
            cards[2].action(communityChestActions);
            expect(player.getMoney()).toBe(1550);
        });

        it("should get 100 when life insurance matures", function () {
            cards[3].action(communityChestActions);
            expect(player.getMoney()).toBe(1600);
        });

        it("should get 20 from income tax refund", function () {
            cards[4].action(communityChestActions);
            expect(player.getMoney()).toBe(1520);
        });

        it("should get 100 from holiday fund", function () {
            cards[5].action(communityChestActions);
            expect(player.getMoney()).toBe(1600);
        });

        it("should inherit 100", function () {
            cards[6].action(communityChestActions);
            expect(player.getMoney()).toBe(1600);
        });

        it("should recieve consultancy fee 25", function () {
            cards[7].action(communityChestActions);
            expect(player.getMoney()).toBe(1525);
        });

        it("should pay hospital fee 100", function () {
            cards[8].action(communityChestActions);
            expect(player.getMoney()).toBe(1400);
        });

        it("bank error in your favor should get 200", function () {
            cards[9].action(communityChestActions);
            expect(player.getMoney()).toBe(1700);
        });

        it("should pay school fee 50", function () {
            cards[10].action(communityChestActions);
            expect(player.getMoney()).toBe(1450);
        });

        it("should pay doctor fee 50", function () {
            cards[11].action(communityChestActions);
            expect(player.getMoney()).toBe(1450);
        });

        it("should get 10 from each player", function () {
            cards[12].action(communityChestActions);
            expect(player.getMoney()).toBe(1510);
            expect(players[0].getMoney()).toBe(1490);
        });

        it("should get 200 in advance to go", function () {
            cards[13].action(communityChestActions);
            expect(player.getMoney()).toBe(1700);
        });

        it("should get 0 street repairs if he own 0 houses and 0 hotels", function () {
            cards[14].action(communityChestActions);
            expect(player.getMoney()).toBe(1500);
        });
    })
});