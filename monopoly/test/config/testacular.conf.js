basePath = '../';

files = [
  JASMINE,
  JASMINE_ADAPTER,
    '../app/components/angular/angular.js',
    '../app/components/angular-route/angular-route.js',
    '../app/components/angular-mocks/angular-mocks.js',
    '../app/**/init.js',
    '../app/monopoly/**/*.js',
    'unit/**/*.js'
];

singleRun = true;

process.env['PHANTOMJS_BIN'] = 'node_modules/.bin/phantomjs';
browsers = ['PhantomJS'];

reporters = ['dots', 'junit'];
junitReporter = {
  outputFile: 'output/unit.xml',
  suite: 'unit'
};
