var Monopoly = require('../model/game.js').Monopoly;
var Properties = require('../model/property.js').Properties;
var UUID = require('../utility/uuid.js').UUID;
var games = [];
var gameController = {};

gameController.createGame = function(data){
    var properties = Properties.createProperties();
    var newGame = Monopoly.createGame(data.gameName, data.noOfPlayers, properties);
    var gameId = UUID.generateUuid();
    games[gameId] = newGame;
    return gameId;
};

gameController.getGame = function(gameId){
    return games[gameId];
};

exports.gameController = gameController;
