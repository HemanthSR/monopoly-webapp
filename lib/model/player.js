var Players = {};
exports.Players = Players;
Players.createPlayer = function (playerId, playerName, color) {
    return new Player(playerId, playerName, color);
}

var Player = function (playerId, playerName, _color) {
    var id = playerId;
    var playerName = playerName;
    var color = _color;

    this.getPlayerInfo = function(){
        return {
            id: id,
            name: playerName,
            color: color
        };
    }
}