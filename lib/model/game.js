var Monopoly = {};
Monopoly.createGame = function (gameName, noOfPlayers, properties) {
    return new Game(gameName, noOfPlayers, properties);
};
exports.Monopoly = Monopoly;

var Game = function (gameName, _noOfPlayers, _properties) {
    var name = gameName;
    var noOfPlayers = _noOfPlayers;
    var properties = _properties;
    var players = [];

    this.generateGameSnapshot = function () {
        return {
            name: name,
            noOfPlayers: noOfPlayers,
            properties: this.generatePropertiesSnapshot(),
            players: this.generatePlayersSnapshot()
        };
    };

    this.generatePropertiesSnapshot = function () {
        var propertiesSnapshot = [];
        properties.forEach(function (property) {
            propertiesSnapshot.push(property.generatePropertySnapshot());
        });
        return propertiesSnapshot;
    };

    this.generatePlayersSnapshot = function () {
        var playersSnapshot = [];
        players.forEach(function (player) {
            playersSnapshot.push(player.generatePlayerSnapshot());
        });
        return playersSnapshot;
    };

    this.getGameInfo = function () {
        return {
            name: name,
            noOfPlayers: noOfPlayers
        };
    };

    this.getPlayersInfo = function () {
        var playersInfo = [];
        players.forEach(function (player) {
            playersInfo.push(player.getPlayerInfo());
        });
        return playersInfo;
    };

    this.joinGame = function (player) {
        players.push(player);
    };

    this.haveRoomForPlayer = function(){
        return noOfPlayers > players.length;
    }
};