var Properties = {};
exports.Properties = Properties;

Properties.createProperties = function () {
    var properties = [];
    properties.push(new Property(0, "Go"));
    properties.push(new Property(1, "Old Kent Road", 3, 60, 2, 10, 30, 90, 160, 250));
    properties.push(new Property(2, "Community Chest", -1));
    properties.push(new Property(3, "Whitechapal Raod", 3, 60, 4, 20, 60, 180, 320, 450));
    properties.push(new Property(4, "City Tax", 0, 200));
    properties.push(new Property(5, "King Cross Station", 1, 200));
    properties.push(new Property(6, "The Angle Islington", 4, 100, 6, 30, 90, 270, 400, 550));
    properties.push(new Property(7, "Chance", -2));
    properties.push(new Property(8, "Euston Road", 4, 100, 6, 30, 90, 270, 400, 550));
    properties.push(new Property(9, "Pentonville Road", 4, 120, 8, 40, 100, 300, 450, 600));
    properties.push(new Property(10, "Just Visiting"));
    properties.push(new Property(11, "Pall Mall", 5, 140, 10, 50, 150, 450, 625, 750));
    properties.push(new Property(12, "Electric Company", 2, 150));
    properties.push(new Property(13, "White Hall", 5, 140, 10, 50, 150, 450, 625, 750));
    properties.push(new Property(14, "Northumberl'D Avenue", 5, 160, 12, 60, 180, 500, 700, 900));
    properties.push(new Property(15, "Marylebone Station", 1, 200));
    properties.push(new Property(16, "Bow Street", 6, 180, 14, 70, 200, 550, 750, 950));
    properties.push(new Property(17, "Community Chest", -1));
    properties.push(new Property(18, "Marlborough Street", 6, 180, 14, 70, 200, 550, 750, 950));
    properties.push(new Property(19, "Vine Street", 6, 200, 16, 80, 220, 600, 800, 1000));
    properties.push(new Property(20, "Free Parking"));
    properties.push(new Property(21, "The Strand", 7, 220, 18, 90, 250, 700, 875, 1050));
    properties.push(new Property(22, "Chance", -2));
    properties.push(new Property(23, "Fleet Street", 7, 220, 18, 90, 250, 700, 875, 1050));
    properties.push(new Property(24, "Trafalgar Square", 7, 240, 20, 100, 300, 750, 925, 1100));
    properties.push(new Property(25, "Fenchurch Street Station", 1, 200));
    properties.push(new Property(26, "Leicester Square", 8, 260, 22, 110, 330, 800, 975, 1150));
    properties.push(new Property(27, "Coventry Street", 8, 260, 22, 110, 330, 800, 975, 1150));
    properties.push(new Property(28, "Water Works", 2, 150));
    properties.push(new Property(29, "Piccadilly", 8, 280, 24, 120, 360, 850, 1025, 1200));
    properties.push(new Property(30, "Go to Jail"));
    properties.push(new Property(31, "Regent Street", 9, 300, 26, 130, 390, 900, 110, 1275));
    properties.push(new Property(32, "Oxford Street", 9, 300, 26, 130, 390, 900, 110, 1275));
    properties.push(new Property(33, "Community Chest", -1));
    properties.push(new Property(34, "Bond Street", 9, 320, 28, 150, 450, 1000, 1200, 1400));
    properties.push(new Property(35, "Liverpool Street Station", 1, 200));
    properties.push(new Property(36, "Chance", -2));
    properties.push(new Property(37, "Park Lane", 10, 350, 35, 175, 500, 1100, 1300, 1500));
    properties.push(new Property(38, "Luxury Tax", 0, 100));
    properties.push(new Property(39, "Mayfair", 10, 400, 50, 200, 600, 1400, 1700, 2000));

    return properties;
}

var Property = function (_propertyId, _propertyName, _groupNumber, _propertyPrice, _baserent, _rentWith1House, _rentWith2Houses, _rentWith3Houses, _rentWith4Houses, _rentWithHotel) {
    var id = _propertyId;
    var name = _propertyName;
    var groupNumber = _groupNumber;
    var price = _propertyPrice;
    var rent = {
        0: _baserent || 0,
        1: _rentWith1House || 0,
        2: _rentWith2Houses || 0,
        3: _rentWith3Houses || 0,
        4: _rentWith4Houses || 0
    };
    var rentWithHotel = (_rentWithHotel || 0);
    var owner = {};

    this.generatePropertySnapshot = function(){
        return {
            id: id,
            name: name,
            groupNumber: groupNumber,
            owner: owner
        }
    }
}