'use strict';

angular.module('monopoly.game')
    .factory('clientService', ['$rootScope', function ($rootScope) {
        var gameId;
        var playerId;

        var joinedToGame = function (_gameId, _playerId) {
            gameId = _gameId;
            playerId = _playerId;
        };

        var getGameId = function(){
            return gameId;
        };

        var getPlayerId = function(){
            return playerId;
        };

        return{
            joinedToGame: joinedToGame,
            getGameId: getGameId,
            getPlayerId: getPlayerId
        }
    }]);