'use strict';

angular
    .module('Monopoly', ['ngRoute', 'monopoly.game'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'joinGame.jade',
                controller: 'JoinGameController'
            }).
            otherwise({
                templateUrl: 'error.jade'
            });
    }]);