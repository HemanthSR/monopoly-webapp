'use strict';

angular.module('monopoly.game')
    .controller('JoinGameController', ['$scope', '$rootScope', '$http', 'clientService',
        function ($scope, $rootScope, $http, clientService) {
            $scope.playerName = "Hemanth";
            $scope.selectedGame = undefined;
            $scope.selectedColor = undefined;
            $scope.games = [];
            $scope.players = [];
            $scope.colors = ['Black', 'White', 'Tomato', 'Darkviolet', 'Darkturquoise', 'Peru'];

            function getGamesList() {
                $http.post('/getGamesList')
                    .success(function (data) {
                        $scope.games = data;
                    });
            }

            function getPlayersList() {
                console.log(clientService.getGameId());
                $http.post('/getPlayersList', {gameId: clientService.getGameId()})
                    .success(function (data) {
                        $scope.players = data;
                        console.log($scope.players);
                    });
            }

            getGamesList();
            var getGamesListIntervalId = setInterval(getGamesList, 3000);
            var getPlayersListIntervalId;

            $scope.joinGame = function () {
                var playerInfo = {
                    playerName: $scope.playerName,
                    selectedGameId: $scope.selectedGame.id,
                    color: $scope.selectedColor
                };

                $http.post('/joinGame', playerInfo).success(function (data) {
                    clientService.joinedToGame(data.gameId, data.playerId);
                    getPlayersList();
                    getPlayersListIntervalId = setInterval(getPlayersList, 3000);
                });
                clearInterval(getGamesListIntervalId);
            };

            $scope.gameSelected = function (game) {
                $scope.selectedGame = game;
            };

            $scope.colorSelected = function (color) {
                $scope.selectedColor = color;
            };
        }]);
