'use strict';

angular
    .module('Monopoly', ['ngRoute', 'monopoly.game'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'createGame.jade',
                controller: 'CreateGameController'
            }).
            when('/gameInfo', {
                templateUrl: 'gameInfo.jade',
                controller: 'GameInfoController'
            }).
            otherwise({
                templateUrl: 'error.jade'
            });
    }]);