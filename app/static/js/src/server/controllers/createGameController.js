'use strict';

angular.module('monopoly.game')
    .controller('CreateGameController', ['$scope', '$rootScope', '$http', '$location', 'serverService',
        function ($scope, $rootScope, $http, $location, serverService) {
            $scope.gameName = "Monopoly";
            $scope.noOfPlayers = undefined;

            $scope.createGame = function () {
                var url = "/createGame";
                var requestParams = {"gameName": $scope.gameName, "noOfPlayers": $scope.noOfPlayers};
                $http.post(url, requestParams)
                    .success(function (data) {
                        serverService.gameCreated(data.gameId);
                        $location.url('gameInfo');
                    }).error(function(){
                        $location.url('error');
                    })
            }
        }]);
