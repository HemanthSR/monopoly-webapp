'use strict';

angular.module('monopoly.game')
    .controller('GameInfoController', ['$scope', '$rootScope', '$http', '$location', 'serverService',
        function ($scope, $rootScope, $http, $location, serverService) {
            var requestParams = {"gameId": serverService.getGameId()};
            $scope.gameInfo = {};
            $http.post('/getGameInfo', requestParams)
                .success(function (data) {
                    $scope.gameInfo = data;
                    console.log($scope.gameInfo);
                }).error(function () {
                    $location.url('error');
                });

            $scope.getClass = function (property) {
                return "prop"+property.id;
            }
        }
    ]);
