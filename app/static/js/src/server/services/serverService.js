'use strict';

angular.module('monopoly.game')
    .factory('serverService', ['$rootScope', function ($rootScope) {
        var gameId;

        var gameCreated = function (_gameId) {
            gameId = _gameId;
        };

        var getGameId = function(){
            return gameId;
        };

        return{
            gameCreated: gameCreated,
            getGameId: getGameId
        }
    }]);