var express = require('express');
var jade = require('jade');
var http = require('http');
var expressApp = express();
var server = http.createServer(expressApp).listen(3000);
var io = require('socket.io').listen(server);
var routes = require('./routes');
var api = require('./routes/api');
var Monopoly = require('./lib/model/game.js').Monopoly;
var Properties = require('./lib/model/property.js').Properties;
var Players = require('./lib/model/player.js').Players;
var UUID = require('./lib/utility/uuid.js').UUID;
var games = {};

expressApp.set('view engine', jade);
expressApp.set("view options", { layout: false });
expressApp.use(express.static('./'));
expressApp.set('views', "./app/views");

expressApp.get('/', routes.index);

expressApp.get('/server', function (req, res) {
    res.render('server/server.jade');
});

expressApp.get('/client', function (req, res) {
    res.render('client/client.jade');
});

expressApp.post('/createGame', function (req, res) {
    var gameId;
    req.on('data', function (data) {
        var requestParams = JSON.parse(data);
        var newGame = Monopoly.createGame(requestParams.gameName, requestParams.noOfPlayers, Properties.createProperties());
        gameId = UUID.generateUuid();
        games[gameId] = newGame;
        res.send({gameId: gameId});
    });
});

expressApp.post('/joinGame', function (req, res) {
    var playerId;
    req.on('data', function (data) {
        var requestParams = JSON.parse(data);
        playerId = UUID.generateUuid();
        var player = Players.createPlayer(playerId, requestParams.playerName, requestParams.color);
        games[requestParams.selectedGameId].joinGame(player);
        res.send({playerId: playerId, gameId: requestParams.selectedGameId});
    });
});

expressApp.post('/getGameInfo', function (req, res) {
    var game;
    req.on('data', function (data) {
        var requestParams = JSON.parse(data);
        game = games[requestParams.gameId];
        res.send(game.generateGameSnapshot());
    });
});

var getAllGamesInfo = function () {
    var gamesInfo = [];
    for (var key in games) {
        if (games[key].haveRoomForPlayer()) {
            var gameInfo = {id: key, game: games[key].getGameInfo()};
            gamesInfo.push(gameInfo);
        }
    }
    return gamesInfo;
};

var getAllPlayersInfo = function (gameId) {
    return games[gameId] && games[gameId].getPlayersInfo();
};

expressApp.post('/getGamesList', function (req, res) {
    res.send(getAllGamesInfo());
});

expressApp.post('/getPlayersList', function (req, res) {
    req.on('data', function (data) {
        var requestParams = JSON.parse(data);
        res.send(getAllPlayersInfo(requestParams.gameId));
    });
});

expressApp.get('/client/:name', routes.client);
expressApp.get('/server/:name', routes.server);

io.sockets.on('connection', function (socket) {
    socket.on('setPseudo', function (data) {
        socket.set('pseudo', data);
    });

    socket.on('message', function (message) {
        socket.get('pseudo', function (error, name) {
            var data = { 'message': message, pseudo: name };
            socket.broadcast.emit('message', data);
            console.log("user " + name + " send this : " + message);
        })
    });
});