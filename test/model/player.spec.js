var assert = require("assert");
var Players = require('../../lib/model/player.js').Players;

describe('Player - ', function () {
    it("Should create Player", function(){
        var player = Players.createPlayer(101, 'name', 'color');
        assert.equal(101, player.getPlayerInfo().id);
    })
});