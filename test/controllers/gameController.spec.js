var assert = require("assert");
var gameController = require('../../lib/controllers/gameController.js').gameController;

describe('Game Controller - ', function () {
    var data = {
        gameName: "name",
        noOfPlayers: 2
    };

    it("should create game", function(){
        var gameId = gameController.createGame(data);
        assert.notEqual(undefined, gameId);

        var game = gameController.getGame(gameId);
        assert.notEqual(undefined, game);

        var gameInfo = game.getGameInfo();
        assert.equal("name", gameInfo.name);
        assert.equal(2, gameInfo.noOfPlayers);
    });
});